Optimized SLERP
===============

SLERP is a rotation iteration method for quaternions (Spherical Linear intERPolation). It is called very frequently in
computer 3D animation to calculate interpolated skeletons of models, therefore efficiency is the outermost importance.
(One of the most impressive work on the topic, I've ever read, "Quaternions, Interpolation and Animation" from Dam, Koch
and Lillholm unfortunately focuses on off-screen rendering only, placing the precision ahead of rendering speed.)

And most people doesn't seem to care, they just copy'n'paste [this horrible code](http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/slerp/index.htm),
not thinking about how computers actually work. No wonder their animation code is slow as a pregnant snail.

The Original Code
-----------------
```c
quat slerp(quat qa, quat qb, double t) {
    quat qm = new quat();
    double cosHalfTheta = qa.w * qb.w + qa.x *qb.x + qa.y *qb.y + qa.z *qb.z;
    if (abs(cosHalfTheta) >= 1.0){
        qm.w = qa.w;qm.x = qa.x;qm.y = qa.y;qm.z = qa.z;
        return qm;
    }
    double halfTheta = acos(cosHalfTheta);
    double sinHalfTheta = sqrt(1.0 - cosHalfTheta*cosHalfTheta);
    if(fabs(sinHalfTheta) < 0.001){
        qm.w = (qa.w * 0.5 + qb.w * 0.5);
        qm.x = (qa.x * 0.5 + qb.x * 0.5);
        qm.y = (qa.y * 0.5 + qb.y * 0.5);
        qm.z = (qa.z * 0.5 + qb.z * 0.5);
        return qm;
    }
    double ratioA = sin((1 - t) * halfTheta) / sinHalfTheta;
    double ratioB = sin(t * halfTheta) / sinHalfTheta;
    qm.w = (qa.w * ratioA + qb.w * ratioB);
    qm.x = (qa.x * ratioA + qb.x * ratioB);
    qm.y = (qa.y * ratioA + qb.y * ratioB);
    qm.z = (qa.z * ratioA + qb.z * ratioB);
    return qm;
}
```
What's wrong with this code? First, it's incorrect. Most of the people figures this out, and replaces the `* 0.5` with the correct
LERP arguments: `ratioA = 1 - t; ratioB = t;`. They also tend to read the comments and fix the sign issue by changing qb's sign:
```c
if (cosHalfTheta < 0) {
    qb.w = -wb.w; qb.x = -wb.x; qb.y = -wb.y; qb.z = -wb.z;
    cosHalfTheta = -cosHalfTheta;
}
```
Okay, now the algorithm is correct, however increadibly inefficient, yet many people use this very primitive version.

Initial Optimisation
--------------------

Why is it so slow? It has several reasons. Mostly that's because it's using expensive square root calculations.
I've seen people trying to fix this by optimizing the sqrt() function. Let's not go mad, instead recall some
of the basic trigonometry equivalencies you learned at primary school.

Second, there's no need to use double precision here when your code uses float coordinates anyway. So replace variables
and all trigonometric functions with their float equivalents: double -> float, acos -> acosf, sin -> sinf etc.

Third, division is also expensive. It's much faster to multiply with the reciprocal, especially when we have to do that twice.

Fourth, that sign change is very expensive, as it changes the sign of the entire quaternion, however it would
be enough to change the result's sign.

These are the optimisations I've seen in most so called "optimised" SLERP implementations. The code now looks like:

__THIS IS THE VERSION (OR VARIATION OF IT) THAT MOST PEOPLE USE__

```c
quat slerp(quat qa, quat qb, float t) {
    quat qm = new quat();
    float cosHalfTheta = qa.w * qb.w + qa.x *qb.x + qa.y *qb.y + qa.z *qb.z;
    if (fabsf(cosHalfTheta) >= 1.0){
        qm.w = qa.w;qm.x = qa.x;qm.y = qa.y;qm.z = qa.z;
        return qm;
    }
    float ratioA;
    float ratioB;
    if(fabs(cosHalfTheta) > 0.999){
        ratioA = 1.0 - t;
        ratioB = t;
    } else {
        float halfTheta = acosf(cosHalfTheta);
        float sinHalfThetaRec = 1 / sinf(halfTheta);
        ratioA = sinf((1 - t) * halfTheta) * sinHalfThetaRec;
        ratioB = sinf(t * halfTheta) * sinHalfThetaRec;
        if(cosHalfTheta < 0.0) {
            ratioB = -ratioB;
        }
    }
    qm.w = (qa.w * ratioA + qb.w * ratioB);
    qm.x = (qa.x * ratioA + qb.x * ratioB);
    qm.y = (qa.y * ratioA + qb.y * ratioB);
    qm.z = (qa.z * ratioA + qb.z * ratioB);
    return qm;
}
```

### Sidenote on the "Official" Implementation

Let's compare this to GLM's implementation, another extremely popular version. It is called `mix`, and it's defined in
*glm/ext/quaternion_common.inl*:
```c
template<typename T, qualifier Q>
GLM_FUNC_QUALIFIER qua<T, Q> mix(qua<T, Q> const& x, qua<T, Q> const& y, T a)
{
    T const cosTheta = dot(x, y);
    if(cosTheta > static cast<T>(1) - epsilon<T>())
    {
        return qua<T, Q>(
            mix(x.w, y.w, a),
            mix(x.x, y.x, a),
            mix(x.y, y.y, a),
            mix(x.z, y.z, a));
    }
    else
    {
        T angle = acos(cosTheta);
        return (sin((static cast<T>(1) - a) * angle) * x + sin(a * angle) *y) / sin(angle);
    }
}
```
First we notice that it does not check the validity of the input. The variable `a` can be greater or equal to 1.0, there's no
short codepath. This is reasonable to an extent, because you can check that before you call slerp a hundred times. I will adopt
this optimisation. Second, it does not take into account that `cosTheta` could be negative, therefore that "if" will fail, and it
will take the longer execution path in cases when LERP should have been used. Also there's no sign check for `ratioB`. This
means GLM's implementation is not error-proof and very input data set specific. This is unacceptable for me.

Good point that you can use that template with `float`s, which would fasten up the execution, but we already knew that. Bad point
that it uses division and it does not cache sin() calls.

Digging Deeper
--------------

Now let's examine how we could make this code much faster in a cross-platform way with a very minimal knowledge on how the
compilers and computers work.

First, throw away C++. You heard me. If you want performance, don't use OOP. The biggest problem with it is, that it allocates
a new quaternion in every single call. That's highly ineffective. It would be better to allocate the resulting quaternion array
in one step, and just fill up the elements in-place with several slerp calls.

Also accessing object members may or may not be compiled efficiently (greatly depends on how you implement your class), so let's
do a favour to the compiler, and remove objects. Notice that the ordering of the components in the quaternion doesn't matter,
all operations are commutative (from the component's perspective). So we can simply pass a pointer to 4 floats, not caring about
whether it's declared as w,x,y,z or x,y,z,w in the C++ class / C structure. If you've implemented quaternions in a way that its
components are not expressed as contiguous 4 floats in memory, then I highly recommend to rewrite your code, because slerp
performance should be the least of your concerns.

Using less variable is always better, and allocates less memory on the stack. This is specially true if you compile for x86_64
which creates a so called "[red-zone](https://en.wikipedia.org/wiki/Red_zone_%28computing%29)" on the stack. If all of your local
variables fit into that, then the compiler will generate a much more optimised and effective code. Moreover, the optimizer can
allocate less registers for less variables, meaning there'll be less MOV/LOAD/STORE instructions, resulting in a smaller and faster
code. So let's put all of those `cosHalfTheta`, `ratioA`, `ratioB` etc. variables into as few temporary variables as possible, and
allocate them at once at the beginning of the function.

For further reduced code size, we calculate sub-results in advance, and reuse them as many times as possible. Conveniently those
are the same values that we need for LERP.

So put everuthing into practice, our final result is like this:
```c
void slerp(float *qa, float *qb, float t, float *ret) {
    float a = 1.0 - t, b = t, d = qa[0] * qb[0] + qa[1] * qb[1] + qa[2] * qb[2] + qa[3] * qb[3], c = fabsf(d);
    if(c < 0.999) {
        c = acosf(c);
        b = 1 / sinf(c);
        a = sinf(a * c) * b;
        b *= sinf(t * c);
        if(d < 0) b = -b;
    }
    ret[0] = qa[0] * a + qb[0] * b;
    ret[1] = qa[1] * a + qb[1] * b;
    ret[2] = qa[2] * a + qb[2] * b;
    ret[3] = qa[3] * a + qb[3] * b;
}
```
As you can see, this code is better than GLM's, as it checks signs. And it is definitely looking much better than the initial
code.

You can download the [source code](https://gitlab.com/bztsrc/slerp-opt/blob/master/slerp_cross.c) for this version.

Let's See How Deep The Rabbit Hole Goes
---------------------------------------

If you're a computer expert and enthusianist like me, you would know that this cross-platform version is still highly unoptimal,
there's a lot we can do to make it faster. However these optimisations require intrinsics, therefore they are not necessarily
cross-platform or cross-build-environment compatible. Regadless I'll try to do my best so that you can compile it with gcc,
Clang and MSVC; under Linux and Windows as well. Converting them into ARM NEON intrinsics should be straight-forward.

Now let's compile our code and take a peek what the compiler actually did with our code:
```
gcc -O3 -ffast-math -msse4 slerp_cross.c -o slerp -lm
objdump -d slerp | less
```

First thing we notice that there's a frame stack. We only allocate a fixed sized memory on the stack (4 floats), and we call
only leaf functions, so we don't need a stack frame, get rid of it. Unfortunately we can't tell a compiler to use this optimisation
exactly on one single function, we have to pass it on the command line.
```
gcc -O3 -ffast-math -fomit-frame-pointer -msse4 slerp_cross.c -o slerp -lm
objdump -d slerp | less
```
Much better, now we can start the real work.

### Optimizing Dot Product

Developers tend to think that compiler optimizers are good, better than a programmer. They are WRONG. Very very (very) WRONG.

We can clearly see that here, as gcc was smart enough to use SIMD, however it failed to recognize the pattern in the dot
product calculation, and therefore used 4 independent scalar multiplications. We can do better! Let's write the dot product's
scheme in a way that makes it clear:
```
      a0   a1   a2   a3
      *    *    *    *
      b0   b1   b2   b3
      =    =    =    =
dot = d0 + d1 + d2 + d3
```
Since a SIMD register can hold 4 packed single precision scalars, we can put the entire quaternion in a single SIMD register.
(And that's how you store quaternions in SIMD, not like [zeux](https://zeux.io/2016/05/05/optimizing-slerp) suggested, because
this way we can avoid that whole "AoSoA" madness. I'd like to point out we can only do this because neither of the SLERP required
quaternion operations care about the ordering of the components. This is a special case.)
```c
__m128 ta = _mm_loadu_ps(qa);
__m128 tb = _mm_loadu_ps(qb);
```
It's not just with SSE we can considerably fasten up the dot product calculation, even more, SSE4.1 has a dot product instruction
that does exactly that!

```
__m128 t1 = _mm_dp_ps(ta, tb, 0xF1);
d = _mm_cvtss_f32(t1);
```
Looks better than 4 individual mulss and an addss, right? If you don't have SSE4.1 don't you worry, I've implemented the SSE1
mulps and addps versions too as a fallback.

### Optimizing LERP

Another place in our code that cries for SIMD is the LERP part at the end. Now that we have loaded our quaternions into SIMD
registers, this should be a piece of cake. We only need to load the ratios, multiply each component with that...
```
      a0   a1   a2   a3           b0   b1   b2   b3
      *    *    *    *            *    *    *    *
      ra   ra   ra   ra           rb   rb   rb   rb
      =    =    =    =            =    =    =    =
ta =  a0   a1   a2   a3     tb =  b0   b1   b2   b3

```
...and add the results together.
```
      a0   a1   a2   a3
      +    +    +    +
      b0   b1   b2   b3
      =    =    =    =
ta =  a0   a1   a2   a3
```
In C with intrinsics this looks like:
```c
t1 = _mm_load1_ps(&a);
ta = _mm_mul_ps(ta, t1);

t1 = _mm_load1_ps(&b);
tb = _mm_mul_ps(tb, t1);

ta = _mm_add_ps(ta, tb);
```
As a last step, we store the result into memory, and we're done!
```c
_mm_storeu_ps(ret, ta);
```
It wasn't some kind of magic, wasn't it? Would it be wrong to assume that with `-O3 -msse4` the compiler's optimizer should have
done these on it's own (specially the dot product)?

### Optimizing Trigonometry Calculations

I'm afraid, that's the end of the road. (Unless we go into Assembly, which I'd better avoid as I wanted a compiler independent
solution.) You can see that `fabsf` is optimized away (there's no library call for it), however `acosf` and `sinf` were not. No
matter how I've tried, I was unable to get gcc to compile these into their native instructions. And library function calls are
very bad for performance. They require two long-distance jumps, which forces the CPU to flush it's internal instruction
cache twice. And we have 4 of those calls in our little slerp function (one acosf and three sinf).

I'm well aware that there's an FSIN FPU instruction, its opcode is D9 FE, which should been supported by `__builtin_sinf()`.
At least that's what the [GNU gcc documentation](https://gcc.gnu.org/onlinedocs/gcc/Other-Builtins.html) says.
```
There are also built-in versions of the ISO C99 functions acosf, ... sinf, ... that are recognized in any mode as ISO C90 reserves
these names for the purpose to which ISO C99 puts them. All these functions have corresponsing versions prefixed with __builtin_.
```
Well, they are definitely not recognized, no matter what compiler options I use. Only `fabsf` works. Same problem with LLVM Clang.

Going further, according to the [Intel Intrinsics Guide](https://software.intel.com/sites/landingpage/IntrinsicsGuide), the header
"immintrin.h" should provide `_mm_acos_ps()` and `_mm_sin_ps()`, and they supposed to be standard SSE intrinsics. Guess what? Those
doesn't work either! Granted, there's no direct opcode for them, but still.

It would been great to have `_mm_sin_ps()`, bacause we could have calculated all three sin() values with a single intrinsic. That's
a pity! Not to mention that by liquidating the function calls alltogether, we could have removed all library dependencies, and
also could have marked our function as a leaf. Combined with the fact that we could have then used SIMD registers for ratioA and
ratioB, eliminating local variables entirely, it would probably made the optimizer to avoid all stack access alltogether (input
arguments are already passed to the function in registers in both Win and SysV ABI). A register only function, that would have
made the real performance improvement! Wrapped in a memory prefetch + slerp call loop for all bones, yamm, that's delicious!

Anyway, you can download the [source code](https://gitlab.com/bztsrc/slerp-opt/blob/master/slerp_simd.c) for this (half-ready) version.

If you decide to find a library that provides `_mm_sin_ps()`, then choose wisely, keep in mind that speed is much more important
for real-time skeleton interpolation than precision.

Cheers,

bzt
