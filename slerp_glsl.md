Interpolating skeletons on GPU, dubbed "PFR"
============================================

At the moment, this is just a think-tank, however I'm positive that it is viable once the details are worked out.

The Basic Idea
--------------

Assuming we have skeletons made up of bones with position + oriantation quaternion pairs. We assign two bones at once into a VBO
record (one for the "past" skeleton, one for the "future" skeleton, hereafter referenced as "P" and "F" respectively). We let the
GPU do the math, and we transfer back the resulting position + orientation pair (hereafter "R"). Simple and cool, isn't it?

With Vulkan it's easy to set up a computational shader in a conform way (relatively speaking). With OpenGL not so much. You would
need CUDA or OpenCL libraries which would introduce a new dependency to the application, so let's forget about those entirely.
Instead we try to hack GL to do our bidding (as we have GL dependency anyway).

### The Vertex Shader

This looks trivial.
```c
#version 330 core

layout (location = 0) in vec3 Ppos;     // position of past bone
layout (location = 1) in vec4 Pori;     // orientation of past bone
layout (location = 2) in vec3 Fpos;     // position of future bone
layout (location = 3) in vec4 Fori;     // orientation of future bone
uniform float t;                        // interpolation ratio, 0 - 1

out vec3 tRpos;                         // output for the frament shader
out vec4 tRori;

vec4 slerp(vec4 qa, vec4 qb, float t)
{
    float a = 1 - t;
    float b = t;
    float d = dot(qa, qb);
    float c = d < 0 ? -d : d;
    if(c < 0.999) {
        c = acos(c);
        b = 1 / sin(c);
        a = sin(a * c) * b;
        b *= sin(t * c);
        if(d < 0) b = -b;
    }
    return qa * a + qb * b;
}

void main()
{
    tRpos = mix(Ppos, FPos, t);         // do the thing
    tRori = slerp(Pori, Fori, t);
}
```
(TODO: check if
```c
    float d = dot(qa, qb);
    float c = d < 0 ? -d : d;
    if(c < 0.999) {
        c = acos(c);
        return (qa * sin((1 - t) * c) + qb * sin(t * c)) / sin(c);
    }
    return mix(qa, qb, t);
```
is actually faster. It's more accurate for sure, however does not take care of `b`'s sign.)

Unfortunately the OpenGL spec is plainfully clear about vertex shaders can't have output layouts. Not entirely sure why, because
that's exactly how computational shaders work, so the hardware can surely handle. It would be pretty handy to return view matrix
transformed verteces back for collusion detection when they are calculated for rendering for example. Anyway, in this case there's
an easy workaround.

### The Fragment Shader

We set up a fragment shader for a framebuffer. However we will not use this as a real framebuffer with colors, but we're going to
interpret it's data as returned bone position and orientation pairs. Then the fragment shader would just simply proxy the data:
```c
#version 330 core

in vec3 tRpos;                          // input from the vertex shader
in vec4 tRori;

layout (location = 4) out vec3 Rpos;    // position of the returned bone
layout (location = 5) out vec4 Rori;    // orientation of the returned bone

void main()
{
    Rpos = tRpos;                       // just copy the data
    Rori = tRori;
}
```
This code would interpolate one bone at a time, so if you set up a VBO to include all bones, then presto, you can calculate the
entire middle skeleton pose with a single glDrawArray() call.

Further issues to address:
1. make GPU call the fragment shader only once per vertex (by drawing points instead of triangles).
2. Figure out a shader that could work as a vertex/fragment shader or computational shader (provided if it's a vertex shader, then
the corresponding fragment shader just copies data or vice versa, the dummy vertex shader just passes data and the corresponding
fragment shader calculates. Either way the shader that calculates could compute shader compatible as well.)
3. For Vulkan: create a workgroup in size of the number of bones, and make to calculate the entire skeleton in parallel
with one shader call. Not sure if we can set up workgroup size dynamically.
