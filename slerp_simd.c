/*
 * slerp_simd.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief SIMD optimized SLERP implementation (half-ready)
 * https://gitlab.com/bztsrc/slerp-opt
 *
 * Compile with (with basic SSE):
 *   cc -fomit-frame-pointer slerp_simd.c -o slerp -lm
 *
 * Compile with (with SSE4.1):
 *   cc -fomit-frame-pointer -msse4 -D__SSE41__ slerp_simd.c -o slerp -lm
 *
 */

#include <math.h>
#include <immintrin.h>

/* remove this define if you can make sure of it that you always pass pointers (qa, qb, ret)
 * with addresses on 16 bytes boundaries. That would fasten up slerp SIMD even more. */
#define SLERP_UNALIGNED

/**
 * The SLERP function itself
 */
void slerp(float *qa, float *qb, float t, float *ret) {
    float a = 1.0 - t, b = t, c, d;
    __m128 t1;
    /* load quaternions into xmm registers */
#ifdef SLERP_UNALIGNED
    __m128 ta = _mm_loadu_ps(qa);
    __m128 tb = _mm_loadu_ps(qb);
#else
    __m128 ta = _mm_load_ps(qa);
    __m128 tb = _mm_load_ps(qb);
#endif
    /* calculate dot product */
#ifdef __SSE41__
    t1 = _mm_dp_ps(ta, tb, 0xF1);
#else
    __m128 t2, t3;
    t2 = _mm_mul_ps(ta, tb);
    t3 = _mm_shuffle_ps(t2, t2, _MM_SHUFFLE(2, 3, 0, 1));
    t1 = _mm_add_ps(t2, t3);
    t3 = _mm_movehl_ps(t3, t1);
    t1 = _mm_add_ss(t1, t3);
#endif
    /* check if we need to use LERP or we can do SLERP */
    d = _mm_cvtss_f32(t1);
    c = __builtin_fabsf(d);
    if(c < 0.999) {
        c = __builtin_acosf(c);
        b = 1 / __builtin_sinf(c);
        a = __builtin_sinf(a * c) * b;
        b *= __builtin_sinf(t * c);
        if(d < 0) b = -b;
    }
    /* multiply quaternions with ratioA and ratioB scalars, and add them together */
    t1 = _mm_load1_ps(&a);
    ta = _mm_mul_ps(ta, t1);
    t1 = _mm_load1_ps(&b);
    tb = _mm_mul_ps(tb, t1);
    ta = _mm_add_ps(ta, tb);
    /* finally store the result in ret */
#ifdef SLERP_UNALIGNED
    _mm_storeu_ps(ret, ta);
#else
    _mm_store_ps(ret, ta);
#endif
}

/**
 * Example invocation
 */

/* mimic a C++ class */
typedef struct {
    float w;
    float x;
    float y;
    float z;
    void *virtualfunc1;
    void *virtualfunc2;
} quat;

int main(int argc, char **argv)
{
    /* define our quaternion "objects" */
    quat qa, qb, ret;

    /* convert high level class struct into a float array by casting */
    slerp((float*)&qa, (float*)&qb, 0.75, (float*)&ret);
}
